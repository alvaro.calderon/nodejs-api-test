import ProductService from "./product.service";
import {ProductInterface} from "../../dto/createUser.schema";
import mongoose, {Model} from "mongoose";
import {Category} from "../../dto/getUser.schema";
import ApiError from "../../error/api-error";


afterAll(async () => {
    await mongoose.connection.close()
})

describe('ProductService', () => {
    it('Should be defined', () => {
        expect(ProductService).toBeDefined()
    })
    describe('Testing getProductById', () => {
        it('Should be defined', () => {
            ProductService({} as any, {} as any,{})
            expect(ProductService).toBeDefined()
        })

        it('Should throw an error if the product does not exist', async () => {
            const mockProductModel = {
                exists: jest.fn().mockReturnValue({
                    exec: jest.fn().mockReturnValue(false)
                })

            } as unknown as Model<ProductInterface>
            const mockCategoryModel = {} as unknown as Model<Category>
            const mockProductService = ProductService(mockProductModel, mockCategoryModel,{})
            try{
                await mockProductService.getProductById(1)
            }catch (e: any) {
                expect(e).toBeInstanceOf(ApiError)
                expect(e.message).toBe('Product not found')
            }
        })

        it('Should return the product if it exists', async () => {
            const mockProductModel = {
                exists: jest.fn().mockReturnValue({
                    exec: jest.fn().mockReturnValue(true)
                }),
                findOne: jest.fn().mockReturnValue({
                    exec: jest.fn().mockReturnValue({
                        toObject: jest.fn().mockReturnValue({
                            id: 1,
                            name: 'Product 1',
                            category: 'Category 1',
                            sku: 'SKU 1',
                            description: 'Description 1'
                        })
                    })
                })
            } as unknown as Model<ProductInterface>
            const mockCategoryModel = {
                findOne: jest.fn().mockReturnValue({
                    exec: jest.fn().mockReturnValue({
                        toObject: jest.fn().mockReturnValue({
                            id: 1,
                            name: 'Category 1',
                            description: 'Description 1',
                            discount: 0
                        })
                    })
                })
            } as unknown as Model<Category>
            const axiosMock = {
                get: jest.fn().mockReturnValue({
                    data:{
                        "id":1,
                        "price": "10",
                        "discount": "10"
                    }
                }),
            }
            const mockProductService = ProductService(mockProductModel, mockCategoryModel,axiosMock)
            const product = await mockProductService.getProductById(1)
            expect(product).toEqual({
                id: 1,
                name: 'Product 1',
                category: {
                    id: 1,
                    name: 'Category 1',
                    description: 'Description 1',
                    discount: 0
                },
                sku: 'SKU 1',
                description: 'Description 1',
                price: '10',
                discount: '10'
            })
            expect(mockProductModel.exists).toBeCalledWith({id: 1})
        })

    })
})