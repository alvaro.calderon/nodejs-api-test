import Product from '../../model/Product'
import Category from "../../model/Category";
import ProductService from './product.service'
import axios from "axios";

export default ProductService(Product, Category, axios)