import {ProductInterface} from '../../dto/createUser.schema';
import ApiError from "../../error/api-error";
import {Category, ProductFullInformation, ProductPriceInformation} from "../../dto/getUser.schema";
import NodeCache from "node-cache";
import {Model} from "mongoose";

const myCache = new NodeCache({stdTTL: 10000, checkperiod: 120});
const FIELD_TO_EXCLUDE = "-_id -__v"
let axios: any

export const saveProduct = (modelProduct: Model<ProductInterface>, modelCategory: Model<Category>) => async (product: ProductInterface): Promise<ProductInterface> => {
    await isValidCategory(product.category,  modelCategory)
    product.id = await findMaxId(modelProduct)
    const newProduct = new modelProduct(product)
    const response = await newProduct.save()
    return response.toObject()
}

const isValidCategory = async (category : String, modelCategory: Model<Category>): Promise<void> => {
    const response = await modelCategory.exists({name: category}).exec()
    if (!response) {
        throw new ApiError("Invalid category", 400)
    }
}

export const updateProduct = (modelProduct: Model<ProductInterface>, modelCategory: Model<Category>) => async (productId: number, product: ProductInterface): Promise<ProductInterface> => {
    await isValidCategory(product.category,  modelCategory)
    await validateDocumentExistence(modelProduct, productId)
    await modelProduct.updateOne({id: productId}, product).exec()
    const document = await modelProduct.findOne({id: productId}, FIELD_TO_EXCLUDE).exec()
    myCache.del(productId)
    return document!.toObject()
}

const findMaxId = async (modelProduct: Model<ProductInterface>): Promise<number> => {
    const maxIdObject = await modelProduct.find().sort({id: -1}).limit(1).exec()
    let maxId = 1
    if (typeof maxIdObject !== 'undefined' && maxIdObject.length > 0) {
        maxId = maxIdObject[0].id + 1
    }
    return maxId
}

const getExtraInformation = async (productId: number): Promise<ProductPriceInformation> => {
    let response
    try {
        response = await axios.get(`${process.env.PRODUCT_PRINCE_URL}/${productId}`)
    } catch (e) {
        throw new ApiError("Product not found in external  service", 404)
    }
    return response.data

}

export const getProductById = (modelProduct: Model<ProductInterface>, modelCategory: Model<Category>) => async (productId: number): Promise<ProductFullInformation> => {
    await validateDocumentExistence(modelProduct, productId)
    const product = await getProduct(productId, modelProduct)
    const extraInformation = await getExtraInformation(productId)
    const categoryDocument = await modelCategory.findOne({name: product.category}, FIELD_TO_EXCLUDE).exec()

    return {
        ...product,
        ...extraInformation,
        category: categoryDocument!.toObject(),
    } satisfies ProductFullInformation
}

const getProduct = async (productId: number, modelProduct: Model<ProductInterface>): Promise<ProductInterface> => {
    let field_filter = FIELD_TO_EXCLUDE
    let fields: CacheProduct | null = null
    const isDataInCache = myCache.has(productId)
    if (isDataInCache) {
        field_filter += "-sku -description"
        fields = myCache.get(productId) as CacheProduct
    }
    const productDocument = await modelProduct.findOne({id: productId}, field_filter).exec()
    const product = productDocument!.toObject()
    if (!isDataInCache) {
        myCache.set(productId, {
            sku: product.sku,
            description: product.description
        })
    }
    return {
        sku: fields ? fields.sku : product.sku!,
        description: fields ? fields.description : product.description!,
        id: product.id!,
        name: product.name!,
        category: product.category!
    }
}

const validateDocumentExistence = async (modelProduct: Model<ProductInterface>, productId: number) => {
    const existsUser = await modelProduct.exists({id: productId}).exec()
    if (!existsUser) {
        throw new ApiError('Product not found', 404)
    }
}

interface CacheProduct {
    sku: string
    description: string
}

export default (product: Model<ProductInterface>, category: Model<Category>, axiosObj: any) => {
    axios = axiosObj
    return {
        saveProduct: saveProduct(product, category),
        updateProduct: updateProduct(product, category),
        getProductById: getProductById(product, category)
    }
}