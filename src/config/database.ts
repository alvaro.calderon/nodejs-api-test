import * as mongoose from "mongoose";
import {ConnectionOptions} from "tls";

const MONGO_URI = `${process.env.MONGO_URI}/${process.env.MONGO_DB_NAME}`;

mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
} as ConnectionOptions);

import '../script/mongodb';