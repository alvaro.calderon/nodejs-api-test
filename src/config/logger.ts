import winston from 'winston';
const { createLogger, format } = winston;

const logger = createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { service: 'product-service' },
    transports: [
        new winston.transports.File({filename: 'request.log', level: 'info'}),
    ]
})

export default logger;