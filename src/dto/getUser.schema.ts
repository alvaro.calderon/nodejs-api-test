import {ProductInterface} from "./createUser.schema";

export type ProductFullInformation = ProductInterface & ProductPriceInformation
export interface Category {
    name: string,
    description: string,
    discount: number,
    code: string,
}

export interface ProductPriceInformation{
    id: number;
    price: number;
    discount: number;
}
