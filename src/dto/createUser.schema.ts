import {object, string} from 'yup'

export interface ProductInterface {
    id: number;
    name: string;
    sku: string;
    description: string;
    category: string;

}

export const productSchema = object().shape({
    name: string().required('Name is required').min(1, 'Name must be at least 1 character'),
    sku: string().required('SKU is required').min(1, 'Name must be at least 1 character'),
    description: string().required('Description is required').min(1, 'Name must be at least 1 character'),
    category: string().required('Category is required').min(1, 'Name must be at least 1 character'),
})