import {Request, Response, NextFunction} from "express";
import logger from "../config/logger";

export default () => {
  const getDurationInMilliseconds = (start: [number, number]) => {
    const NS_PER_SEC = 1e9;
    const NS_TO_MS = 1e6;
    const diff = process.hrtime(start);

    return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
  };

  return (req: Request, res: Response, next: NextFunction) => {
    logger.info("Request received: " + req.method + " " + req.originalUrl);
    const start = process.hrtime();
    res.on("close", () => {
      const durationInMilliseconds = getDurationInMilliseconds(start);
      logger.info(
        `${req.method} ${
          req.originalUrl
        } [CLOSED] ${durationInMilliseconds.toLocaleString()} ms`
      );
    });

    next();
  };
};
