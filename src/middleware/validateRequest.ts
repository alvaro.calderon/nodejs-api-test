import {AnySchema} from 'yup'
import {NextFunction, Response, Request} from "express";

const validate = (schema: AnySchema) => async (req: Request, res: Response, next: NextFunction) => {
    try{
        await schema.validate(req.body)
        next()
    }catch (e: any) {
        res.status(400).json({error: e.message})
    }
}

export default validate