import Category from "../model/Category";
import logger from "../config/logger";


const insertDefaultCategories = async () => {
    await Category.deleteMany({}).exec()
    logger.info("Categories deleted successfully")
    await Category.insertMany([
        {
            name: "Electronics",
            description: "Electronics",
            discount: 10,
            code: "CAT01",
        },
        {
            name: "Clothes",
            description: "Clothes",
            discount: 20,
            code: "CAT02",
        },
        {
            name: "Food",
            description: "Food",
            discount: 5,
            code: "CAT03",
        },
        {
            name: "Books",
            description: "Books",
            discount: 0,
            code: "CAT04",
        },
        {
            name: "Toys",
            description: "Toys",
            discount: 15,
            code: "CAT05",
        }
    ])
    logger.info("Categories inserted successfully")
}

insertDefaultCategories()