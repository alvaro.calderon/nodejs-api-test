import * as mongoose from "mongoose";
import {ProductInterface} from "../dto/createUser.schema";


const Schema = mongoose.Schema;
const model = mongoose.model;

const productSchema = new Schema<ProductInterface>({
    id: Number,
    name: String,
    sku: String,
    description: String,
    category: String,
})

const Product = model<ProductInterface>('Product', productSchema);
export default Product;