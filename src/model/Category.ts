import * as mongoose from "mongoose";
import {Category} from "../dto/getUser.schema";


const Schema = mongoose.Schema;
const model = mongoose.model;

const categorySchema = new Schema<Category>({
    name: String,
    description: String,
    discount: Number,
    code: String,
})

const Category = model<Category>('Category', categorySchema);
export default Category;