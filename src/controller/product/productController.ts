import {Request, Response} from 'express';
import ProductService from "../../service/product";

export const getProduct = async (req: Request, res: Response) => {
    let result
    try {
        result = await ProductService.getProductById(Number(req.params.productId))
        res.status(200).json(result)
    } catch (e: any) {
        res.status(e.statusCode).json({error: e.message})
    }
}


export const persistsProduct = async (req: Request, res: Response) => {
    let result
    try {
        result = await ProductService.saveProduct(req.body)
    } catch (e: any) {
        res.status(e.statusCode).json({error: e.message})
    }
    res.status(201).json(result)
}

export const modifyProduct = async (req: Request, res: Response) => {
    let result
    try {
        result = await ProductService.updateProduct(Number(req.params.productId), req.body)
        res.status(200).json(result)
    } catch (e: any) {
        res.status(e.statusCode).json({error: e.message})
    }
}
