import app from '../../app';
import request from "supertest";
import mongoose from "mongoose";
import {ProductInterface} from "../../dto/createUser.schema";
import Product from '../../model/Product';



const Builder = {
    product: ({ name = 'my product', sku = "", description = 'this is a test', category = '' } = {}) => ({
        name,
        description,
        sku,
        category
    }),
}

afterAll(async () => {
    await mongoose.connection.close()

})

beforeAll(async () => {
    await Product.remove({}).exec()
    const product =new Product({ name: "Sony TV 45 inches", sku: "pr123", description: "Sony TV 45 inches with 4K resolution and HDR", category: "Electronics", id: 1 })
    await product.save()
})

describe('GET /product', () => {
    test("should return response 200", async () => {
        await request(app)
            .get('/product/1')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)

    })

    test("should return an object whit id equal to the one passed in the url", async () => {
        const productId = 1
        const response = await request(app)
            .get(`/product/${productId}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .send()
        expect(response.body).toBeInstanceOf(Object)
        expect(response.body).toHaveProperty('id', productId)
    })

});

describe('POST /products', () => {
    describe('should return 400 when body', () => {
        test("does not have all properties or is empty", async () => {
            const fields= [
                {},
                { name: "Sony TV 45 inches" },
                { name: "Sony TV 45 inches", sku: "pr123" },
                { name: "Sony TV 45 inches", sku: "pr123", description: "Sony TV 45 inches with 4K resolution and HDR" },
            ]
            for(const field of fields) {
                await request(app)
                    .post('/product')
                    .send(field)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(400)
            }
        })
    })
    test("should return a 201 ", async () => {
        const product = Builder.product({ name: "Sony TV 45 inches", sku: "pr123", description: "Sony TV 45 inches with 4K resolution and HDR", category: "Electronics" })
        await request(app)
            .post('/product')
            .send(product)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
    })

    test("should return the same product with an id", async () => {
        const product = { name: "Sony TV 45 inches", sku: "pr123", description: "Sony TV 45 inches with 4K resolution and HDR", category: "Electronics" } as ProductInterface
        const response = await request(app)
            .post('/product')
            .send(product)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
        expect(response.body).toHaveProperty('id')
        expect(response.body).toHaveProperty('name', product.name)
        expect(response.body).toHaveProperty('sku', product.sku)
        expect(response.body).toHaveProperty('description', product.description)
        expect(response.body).toHaveProperty('category', product.category)
    })

})