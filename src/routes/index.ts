import {Application} from "express";
import productRoutes from './product';
import healthCheckRoutes from './healthCheck';


export default (app : Application) => {
    app.use('/api/product', productRoutes);
    app.use('/healthcheck', healthCheckRoutes)
}
