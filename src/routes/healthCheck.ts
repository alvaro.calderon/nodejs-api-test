import express from "express";
import {healthCheck} from "../controller/healthcheck/healthchecker";

const router = express.Router();

router.get('/', healthCheck)

export default router;