import express from "express";
import validate from "../middleware/validateRequest";
import {productSchema} from "../dto/createUser.schema";
import {getProduct, modifyProduct, persistsProduct} from "../controller/product/productController";


const router = express.Router();

router.get('/:productId', getProduct);


router.post('/', validate(productSchema) , persistsProduct);

router.put('/:productId', modifyProduct)


export default router;