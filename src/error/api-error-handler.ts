import ApiError from "./api-error";
import { Request, Response } from "express";

const apiErrorHandler = (err: Error | ApiError, req: Request, res: Response) => {
  if(err instanceof ApiError){
    return res.status(err.statusCode).json(err.message)
  }
    return res.status(500).json("Internal Server Error")
}

export default apiErrorHandler