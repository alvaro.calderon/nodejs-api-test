
class ApiError {
    public readonly message: string;
    public readonly statusCode: number;
    constructor(message: string, statusCode = 400) {
        // super(message)
        this.message = message;
        this.statusCode = statusCode;
    }
    static badRequest(error: any) {
        return new ApiError(error.message, 400)
    }
}

export default ApiError