import express from 'express';
import requestLogger from './middleware/requestLogger';
import * as dotenv from 'dotenv';
import path from 'path';
import apiErrorHandler from "./error/api-error-handler";
import routes from "./routes";

//load environment variables
dotenv.config({
    path :  path.resolve(__dirname, `.env.${process.env.NODE_ENV}`)
});

//load database
import './config/database';
import logger from "./config/logger";

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Middleware
app.use(requestLogger());

routes(app);

//Error handler
app.use(apiErrorHandler);

const PORT = process.env.PORT || 3000;
if(process.env.NODE_ENV !== 'test') {
    app.listen(PORT, () => {    
        logger.info('Server started on port 3000');
    })
}

export default app;